#include "CharsProvider.h"


CharsProvider CharsProviders::create(const String& string) {
	return [&](std::function<void(char)> charsReceiver) { 
		size_t length = string.length();
		const char* c_str = string.c_str();
		for (size_t i = 0; i < length; i++) {
			charsReceiver(c_str[i]);
		}
	};
}

CharsProvider CharsProviders::create(const char* chars) {
	return [chars](std::function<void(char)> charsReceiver) { 
		const char* pointer = chars;
		char lastChar;
		do {
			charsReceiver(lastChar = *(pointer++));
		} while (lastChar != 0);
	};
}

CharsProvider CharsProviders::create(const __FlashStringHelper* flashStringHelper) {
	return [flashStringHelper](std::function<void(char)> charsReceiver) {
		PGM_P pointer = reinterpret_cast<PGM_P>(flashStringHelper);
		char lastChar;
		do {
			charsReceiver(lastChar = pgm_read_byte(pointer++));
		} while (lastChar != 0);
	};
}
