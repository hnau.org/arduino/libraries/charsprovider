#ifndef CHARSPROVIDER_H
#define CHARSPROVIDER_H

#include <Arduino.h>


typedef std::function<void(std::function<void(char)>)> CharsProvider;

class CharsProviders {
	
public:
	static CharsProvider create(const String& string);
	static CharsProvider create(const char* chars);
	static CharsProvider create(const __FlashStringHelper* flashStringHelper);

};

#define CP(source) CharsProviders::create(source)
#define FPCP(pstr_pointer) CP(FPSTR(pstr_pointer))
#define FCP(string_literal) CP(F(string_literal))


#endif //CHARSPROVIDER_H
